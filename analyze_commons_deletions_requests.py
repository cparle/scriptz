#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Input:
# - deleted pages as per `query`
# - deletion requests as output by `extract_commons_deletion_requests.py`

import json
import pickle
import re
from collections import Counter, OrderedDict
from typing import List, Set, Tuple

import pandas as pd
from sklearn.cluster import KMeans
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer

from wmfdata.spark import create_session

SNAPSHOT = '2023-05'
START = '2022-05-01'
END = '2023-06-01'
RANDOM_SEED = 1984
N_CLUSTERS = 10
DATA_LAKE_QUERY = f"""
SELECT page_id, revision_id, page_title, event_timestamp, c.comment_text
FROM wmf.mediawiki_history mh
LEFT JOIN wmf_raw.mediawiki_logging l ON mh.page_id = l.log_page
LEFT JOIN wmf_raw.mediawiki_private_comment c ON l.log_comment_id = c.comment_id
WHERE mh.snapshot = '{SNAPSHOT}' AND c.snapshot = '{SNAPSHOT}' AND l.snapshot = '{SNAPSHOT}'
AND event_timestamp >= '{START}' AND event_timestamp < '{END}'
AND event_entity = 'revision'
AND event_type = 'create'
AND log_type = 'delete'
AND log_action = 'delete'
AND page_namespace = 6
AND page_is_redirect IS NULL
AND page_is_deleted
AND mh.wiki_db = 'commonswiki' AND c.wiki_db = 'commonswiki' AND l.wiki_db = 'commonswiki'
AND SIZE(event_user_is_bot_by) <= 0
AND SIZE(event_user_is_bot_by_historical) <= 0
"""


# English stopwords from
# https://raw.githubusercontent.com/Wikidata/soweego/master/soweego/commons/resources/stopwords_eng.txt
def load_stopwords(path: str) -> Set[str]:
    with open(path) as fin:
        stopwords = set([l.rstrip() for l in fin])
    # Add some domain-specific stopwords
    stopwords.update(
        {
            'www', 'http', 'https', 'com', 'org',
            'image', 'photo', 'work', 'works', 'file',
            'commons',
        }
    )

    return stopwords


# Bags of words & wikilinks
def bag_of_words(reasons: List[str], stopwords: Set[str]) -> Tuple[Counter, Counter, List[str]]:
    wikilink_re = re.compile(r'\[\[[^\]]+\]\]')
    tokens, wikilinks, clustering_input = [], [], []
    for r in reasons:
        parts = r.split('[[User:')  # Very rough split on first user occurrence
        actual = parts[0]
        for wl in re.findall(wikilink_re, actual):
            wikilinks.append(wl.split('|')[0].strip('[]'))  # Add wikilink target only
        clean = re.sub(wikilink_re, '', actual).strip()  # Remove wikilinks
        if not clean:
            continue  # Skip empty strings
        clustering_input.append(clean)
        words = (re.split('\W+', clean.casefold()))  # Split on non-word chars
        for w in filter(lambda x: len(x) > 1, words):  # Filter unigrams
            if w not in stopwords:  # Filter stopwords
                tokens.append(w)

    return Counter(tokens), Counter(wikilinks), clustering_input


# Inspired from
# https://scikit-learn.org/stable/auto_examples/text/plot_document_clustering.html#k-means-clustering-on-text-features
def cluster(pre_processed_reasons: List[str], stopwords: Set[str]) -> List[str]:
    out_buffer = []
    # Term frequency vectors
    vectorizer = TfidfVectorizer(strip_accents='unicode', stop_words=list(stopwords), use_idf=False)
    X_tfidf = vectorizer.fit_transform(pre_processed_reasons)
    # Reduce dimension to 100 through LSA
    lsa = make_pipeline(TruncatedSVD(n_components=100, random_state=RANDOM_SEED), Normalizer(copy=False))
    X_lsa = lsa.fit_transform(X_tfidf)
    # K-means clustering
    kmeans = KMeans(n_init='auto', random_state=RANDOM_SEED, n_clusters=N_CLUSTERS)
    kmeans.fit(X_lsa)
    # Retrieve tokens
    original_space_centroids = lsa[0].inverse_transform(kmeans.cluster_centers_)
    order_centroids = original_space_centroids.argsort()[:, ::-1]
    terms = vectorizer.get_feature_names_out()
    for i in range(kmeans.n_clusters):
        print(f"Cluster {i}: ", end="")
        out_buffer.append(f"Cluster {i}: ")
        for ind in order_centroids[i, :10]:
            print(f"{terms[ind]} ", end="")
            out_buffer.append(f"{terms[ind]} ")
        print()
        out_buffer.append('\n')

    return out_buffer


def main() -> None:
    # Deleted pages - `dp`
    # Query the data lake
    spark = create_session(type='yarn-large', app_name='commons-deletions', ship_python_env=True)
    ddp = spark.sql(DATA_LAKE_QUERY)
    dp = ddp.toPandas()
    spark.stop()

    # Group by page ID, get latest revision ID & event timestamp, aggregate page titles & edit messages
    dp.event_timestamp = pd.to_datetime(dp.event_timestamp)
    dp = (
        dp
        .groupby('page_id', as_index=False)
        .agg(
            {
                'revision_id': 'max', 'event_timestamp': 'max',
                'page_title': lambda x: set(x), 'comment_text': lambda x: set(x),
            }
        )
    )
    # 1 deleted page ID may have N page titles & N edit messages
    dp['titles'] = dp.page_title.agg(lambda x: len(x))
    dp['comments'] = dp.comment_text.agg(lambda x: len(x))

    dp = dp.explode('page_title').explode('comment_text')

    # Deletion requests - `dr`
    dr = pd.DataFrame.from_records([json.loads(l.rstrip()) for l in open('/home/mfossati/commons_deletions/deletion_times.jsonl')])
    dr.page_title = dr.page_title.str.replace(' ', '_')
    dr.requested = pd.to_datetime(dr.requested)
    dr.deleted = pd.to_datetime(dr.deleted)
    dr['solved_in'] = dr.deleted - dr.requested
    dr['total_seconds'] = dr.solved_in.dt.total_seconds()

    # Merge with deleted pages: final dataset - `ds`
    ds = dr.merge(dp, on='page_title', how='left')

    # Keep deletion requests that don't match with deleted pages
    not_in_dp = ds[ds.page_id.isna()]
    not_in_dp.to_pickle('drs_not_in_dps.pkl')
    not_in_dp.to_csv('drs_not_in_dps.csv', index=False)

    # Process the final dataset
    ds = ds.dropna()
    ds.total_seconds = ds.total_seconds.astype(int)
    ds.page_id = ds.page_id.astype(int)
    ds.revision_id = ds.revision_id.astype(int)
    ds.titles = ds.titles.astype(int)
    ds.comments = ds.comments.astype(int)
    ds['event_has_dr_link'] = ds.comment_text.str.contains('Commons:Deletion requests/File')

    ds['opening_wikilink'] = ds.opening_reason.str.extract(r'(\[\[COM:[^\]]+\]\])', expand=False)
    ds['closing_wikilink'] = ds.closing_reason.str.extract(r'(\[\[COM:[^\]]+\]\])', expand=False)
    ds['deletion_wikilink'] = ds[~ds.event_has_dr_link].comment_text.str.extract(r'(\[\[COM:[^\]]+\]\])', expand=False)

    opening = ds.opening_reason.str.split('[[User:', n=1, regex=False, expand=True)
    opening = opening.rename(columns={0: 'opening_reason', 1: 'full_user'})
    ou = opening.full_user.str.split('|', n=1, expand=True).drop(columns=[1]).rename(columns={0: 'opening_user'})
    ds = ds.drop(columns=['opening_reason']).join(opening.drop(columns=['full_user']).join(ou))
    closing = ds.closing_reason.str.split('[[User:', n=1, regex=False, expand=True)
    closing = closing.rename(columns={0: 'closing_reason', 1: 'full_user'})
    cu = closing.full_user.str.split('|', n=1, expand=True).drop(columns=[1]).rename(columns={0: 'closing_user'})
    ds = ds.drop(columns=['closing_reason']).join(closing.drop(columns=['full_user']).join(cu))

    # Dump full & readable datasets
    ds.to_pickle('full_dataset.pkl')
    ds.to_csv('full_dataset.csv', index=False)
    readable = ds.filter(
        [
            'page_title', 'solved_in',
            'opening_reason', 'closing_reason', 'deletion_reason',
            'opening_wikilink', 'closing_wikilink', 'deletion_wikilink',
            'total_seconds',
        ]
    )
    readable.to_csv('readable_dataset.csv', index=False)

    # Opening reasons NLP
    stopwords = load_stopwords('stopwords_eng.txt')
    tokens, wikilinks, clustering_input = bag_of_words(ds.opening_reason.to_list(), stopwords)

    with open('tokens.pkl', 'wb') as fout:
        pickle.dump(tokens, fout)
    with open('wikilinks.pkl', 'wb') as fout:
        pickle.dump(wikilinks, fout)
    with open('tokens.json', 'w') as fout:
        json.dump(OrderedDict(tokens.most_common()), fout, indent=2, ensure_ascii=False)
    with open('wikilinks.json', 'w') as fout:
        json.dump(OrderedDict(wikilinks.most_common()), fout, indent=2, ensure_ascii=False)

    clusters_buffer = cluster(clustering_input, stopwords)

    with open('clusters', 'w') as fout:
        fout.write(''.join(clusters_buffer))

if __name__ == '__main__':
    main()
