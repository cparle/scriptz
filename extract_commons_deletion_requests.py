#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import json
import re
from datetime import datetime, timedelta

from wmfdata.spark import create_session
from pyspark.sql import functions as F

COMMONS_NS4_QUERY = """SELECT page_id, revision_id, page_title, revision_text
FROM wmf.mediawiki_wikitext_current
WHERE page_namespace=4 and wiki_db='commonswiki' and snapshot='{}'
"""
TIMESTAMP_RAW_RE = r'([012][0-9]:[0-5][0-9], \d{,2} (January|February|March|April|May|June|July|August|September|October|November|December) \d{4})'
DELETED_RAW_RE = r"('{3}?[Dd]eleted:?'{3}?\s?|\{\{[Dd]eletionFooter\|[Dd][Ee][Ll][Ee][Tt][Ee][Dd]\|?)"
START_RE = re.compile(TIMESTAMP_RAW_RE)
END_RE = re.compile(DELETED_RAW_RE + r'(.*)' + TIMESTAMP_RAW_RE)
OPENING_REASON_RE = re.compile(r'=\n(.*)' + TIMESTAMP_RAW_RE)
OUTPUT = '/home/mfossati/commons_deletions/deletion_times.jsonl'


def day_by_day(start, end):
    while end > start:
        start += timedelta(days=1)

        yield start.strftime('%Y/%m/%d')


def generate_dataset(start_date, end_date, commons_ns4, output_path):
    skipped_archives, skipped_files = 0, 0
    no_requests, no_deletions = 0, 0
    no_opening_reasons, no_closing_reasons = 0, 0
    written = 0

    with open(output_path, 'w', buffering=1) as fout:
        for dt in day_by_day(start_date, end_date):
            print(dt)

            try:
                archive = (
                    commons_ns4
                    .where(commons_ns4.page_title == f'Commons:Deletion requests/Archive/{dt}')
                    .select('revision_text')
                    .collect()[0].revision_text
                )
            except Exception as e:
                print(f'Skipping archive "{archive}": {e}')
                skipped_archives += 1
                continue

            files = archive.split('\n')[1:]
            for f in filter(None, files):
                clean = f.strip('{}')
                try:
                    wikitext = (
                        commons_ns4
                        .where(commons_ns4.page_title == clean)
                        .select('revision_text')
                        .collect()
                    )
                except Exception as e:
                    print(f'Skipping "{clean}": {e}')
                    skipped_files += 1
                    continue

                if not wikitext:
                    print(f'Skipping "{clean}": no wikitext')
                    skipped_files += 1
                    continue

                wikitext = wikitext[0].revision_text

                start = re.search(START_RE, wikitext)
                if not start:
                    skipped_files += 1
                    no_requests += 1
                    continue
                requested = start.group(1)

                closing_reason, deleted = None, None
                end = re.search(END_RE, wikitext)
                if end:
                    try:
                        closing_reason = end.group(2)
                        deleted = end.group(3)
                    except IndexError:
                        print(f'The closing regexp did not work for "{clean}": {wikitext}')
                else:
                    no_closing_reasons += 1
                    no_deletions += 1

                reason = re.search(OPENING_REASON_RE, wikitext)
                if reason:
                    opening_reason = reason.group(1)
                else:
                    no_opening_reasons += 1

                fout.write(
                    json.dumps(
                        {
                            'page_title': f"{clean.split(':')[-1]}",
                            'requested': requested,
                            'deleted': deleted,
                            'opening_reason': opening_reason,
                            'closing_reason': closing_reason,
                        },
                        ensure_ascii=False
                    ) + '\n'
                )
                written += 1

    print(
        f"""
        Skipped archive pages: {skipped_archives}
        Skipped files: {skipped_files}
        No request date found: {no_requests}
        No deletion date found: {no_deletions}
        No opening reason found: {no_opening_reasons}
        No closing reason found: {no_closing_reasons}
        Records written: {written}
        """
    )


def parse_args():
    parser = argparse.ArgumentParser(
        description=(
            'Generate a JSON Lines dataset of Commons deletion requests '
            'with request and deletion time stamps'
        )
    )
    parser.add_argument('snapshot', metavar='YYYY-MM', help='"wmf.mediawiki_wikitext_current" snapshot')
    parser.add_argument('start', metavar='YYYY-MM-DD', help='start from this date')
    parser.add_argument('end', metavar='YYYY-MM-DD', help='until this date')
    parser.add_argument(
        '-o', '--output', metavar='/hdfs_path/to/jsonl', default=OUTPUT,
        help=f'path to output JSON Lines. Default: "{OUTPUT}"'
    )

    return parser.parse_args()


def main():
    args = parse_args()

    spark = create_session(type='yarn-large', app_name='commons-deletions', ship_python_env=True)
    ns4 = spark.sql(COMMONS_NS4_QUERY.format(args.snapshot))
    ns4.cache()

    start = datetime.strptime(args.start, '%Y-%m-%d')
    end = datetime.strptime(args.end, '%Y-%m-%d')

    generate_dataset(start, end, ns4, args.output)

    spark.stop()


if __name__ == '__main__':
    main()
