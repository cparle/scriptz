#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# TODO https://phabricator.wikimedia.org/T311730#8341495

"""Gather section titles to be excluded from the section topics data pipeline.
Input is the source language section alignment dataset, from which we extract alignments for 271 wikis.

Note that the output excludes the source language!
"""

import json
from collections import defaultdict, OrderedDict

from wmfdata.spark import get_session

EXCLUDE = {
    # https://phabricator.wikimedia.org/T318092
    'en': ['external links', 'further reading', 'references'],
    # https://phabricator.wikimedia.org/T279519#7368694
    'fr': [
        'notes', 'références', 'notes et références', 'bibliographie', 'webographie',
        'articles connexes', 'liens externes', 'voir aussi', 'annexes',
    ],
    'es': [
        'fuentes', 'libros', 'periódicos y publicaciones', 'en línea'
    ]
}

spark = get_session(app_name='exclude-sections')

blacklist = defaultdict(set)

for source in EXCLUDE:
    secz = spark.read.parquet(f'/user/mnz/secmap_results/aligned_sections/{source}wiki_aligned_sections_2022-02.parquet')
    translations = (
        secz
        .where(secz.source.isin(EXCLUDE[source]))
        .where(secz.rank == 1)
        .select('source', 'target', 'target_language')
        .orderBy('source')
        .toPandas()
    )

    for row in translations.itertuples():
        blacklist[row.target_language].add(row.target)

blacklist = {k: sorted(list(v)) for k, v in blacklist.items()}  # Make values serializable & sort them
json.dump(OrderedDict(sorted(blacklist.items())), open('/home/mfossati/section-topics/section_topics/section_title_blacklist.json', 'w'), indent=2, ensure_ascii=False)
spark.stop()
